﻿using UnityEngine;

public class Enemy : MonoBehaviour {

    public float MaxHealth;
    private float currentHealth;

    private void Start()
    {
        currentHealth = MaxHealth;
    }

	void ReceiveDamage(float damage) {
		currentHealth -= damage;
		if (currentHealth <= 0) {
			Die();
		}
	}

	void Die() {
		Destroy(this.gameObject);
	}

    private void OnGUI()
    {
        if (currentHealth < MaxHealth)
        {
            Vector3 guiPosition = Camera.main.WorldToScreenPoint(transform.position);
            float width = 50;
            float height = 10;
            float ratio = currentHealth / MaxHealth;
            Color bgColor = ratio >= 0.5 ? Color.green : ratio >= 0.3 ? Color.yellow : Color.red;
            GUI.backgroundColor = bgColor;
            GUI.HorizontalScrollbar(new Rect(guiPosition.x - width/2, Screen.height - guiPosition.y - 30, width, height), 0, currentHealth, 0, MaxHealth);
            GUI.backgroundColor = Color.white;
        }
    }
}
