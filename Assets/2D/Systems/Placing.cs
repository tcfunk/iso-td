﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Placing : MonoBehaviour {

    public bool InPlacementMode;
    public GameObject TowerPrefab;
    public Material GhostTowerMaterial;

    private GameObject ghost;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (InPlacementMode)
        {
            // Place tower if mouse was clicked
            if (Input.GetMouseButtonDown(0))
            {
                BuildTower();
            }

            // Update ghost tower position
            if (ghost)
            {
                Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                ghost.transform.position = pos;
            }
            else
            {
                CreateGhost();
            }
        }
        else
        {
            if (ghost)
            {
                Destroy(ghost.gameObject);
                ghost = null;
            }
        }
	}

    void CreateGhost()
    {
        Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        ghost = Instantiate<GameObject>(TowerPrefab, pos, Quaternion.identity);
        ghost.GetComponentInChildren<SpriteRenderer>().material = GhostTowerMaterial;

        // Disable scripting components for ghost tower
        ghost.GetComponent<Attacking>().enabled = false;
        ghost.GetComponent<Targeting>().enabled = false;
    }

    void BuildTower()
    {
        Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Instantiate<GameObject>(TowerPrefab, pos, Quaternion.identity);
    }
}
