﻿using UnityEngine;

public class Damaging : MonoBehaviour {

	public float Damage;

	void OnCollisionEnter2D(Collision2D other) {
		if (other.gameObject.CompareTag("Enemy")) {
			other.gameObject.SendMessage("ReceiveDamage", Damage);
			Despawn();
		}
	}

	void OnCollisionEnter(Collision other) {
		if (other.gameObject.CompareTag("Enemy")) {
			other.gameObject.SendMessage("ReceiveDamage", Damage);
			Despawn();
		}
	}

    void Despawn() {
		Destroy(this.gameObject);
	}

}
