﻿using UnityEngine;


public class Spawning : MonoBehaviour {

    public GameObject[] Checkpoints;
    public GameObject EnemyPrefab;

    public float Cooldown;
    private float timeLeft;

    // Use this for initialization
    void Start() {
        timeLeft = 0;
    }

    // Update is called once per frame
    void Update() {

        if (timeLeft <= 0) {
            timeLeft = Cooldown;
            GameObject enemy = (GameObject)Instantiate(EnemyPrefab, Checkpoints[0].transform.position, Quaternion.identity);
            enemy.SendMessage("SetCheckpoints", Checkpoints);

        } else {
            timeLeft -= Time.deltaTime;

        }
    }
}
