﻿using UnityEngine;

public class Pathable : MonoBehaviour {

	public GameObject[] Checkpoints;
	public float Speed;
	public float AlmostThere;

	private int CurrentCheckpoint;	
	private GameObject Target;

	
	// Update is called once per frame
	void Update () {
		if (CurrentCheckpoint < Checkpoints.Length) {
			March();
		} else {
			Despawn();
		}
	}

	// Set next target
	void SetCheckpoint(bool firstTime) {
		if (++CurrentCheckpoint >= Checkpoints.Length) return;
		Target = Checkpoints[CurrentCheckpoint];
	}

	void SetCheckpoints(GameObject[] checkpoints) {
		this.Checkpoints = checkpoints;
		CurrentCheckpoint = 0;
		transform.position = Checkpoints[0].transform.position;
		SetCheckpoint(true);
	}

	void March() {
		Vector3 dir = Target.transform.position - transform.position;
		transform.position = transform.position + dir.normalized * Speed * Time.deltaTime;

		if (gameObject.GetComponentInChildren<SpriteRenderer>()) {
            float diff = Vector3.Distance(transform.position, Target.transform.position);
            if (diff <= AlmostThere)
            {
                SetCheckpoint(false);
            }
		}
	}

	void Despawn() {
		Destroy(this.gameObject);
	}

	void OnTriggerEnter(Collider other) {
		if (other.CompareTag("Checkpoint")) {
			SetCheckpoint(false);
		}
	}

    public GameObject GetCurrentCheckpoint()
    {
        if (CurrentCheckpoint < Checkpoints.Length)
        {
            return Checkpoints[CurrentCheckpoint];
        }
        else
        {
            return null;
        }
    }
}
