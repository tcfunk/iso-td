﻿using UnityEngine;
using System.Collections.Generic;

public class Attacking : MonoBehaviour {

    public enum AttackMode
    {
        STANDARD,
        MULTIPLE,
        CHAINING
    };

    public enum TargetingPriority { First, Last, Weakest, Strongest, Fixed };

    public float Cooldown;
    public GameObject Projectile;
    public GameObject Barrel;
    public float ProjectileSpeed;

    public AttackMode Mode;
    public TargetingPriority Priority;

    /// <summary>
    /// Maximum number of targets acquirable by the tower if in MULTIPLE target mode, OR
    /// maximum number of jumps that a CHAINING attack will take
    /// </summary>
    public int MaxTargets = 1;

    // These pertain specificly to chaining attacks
    int JumpsLeft;
    GameObject PreviousTarget;

    /// <summary>
    /// Attack range of the tower
    /// </summary>
    public float Range;

    /// <summary>
    /// Time left until next attack tick
    /// </summary>
    private float TimeLeft;

    // Use this for initialization
    void Start() {
        TimeLeft = 0;
        JumpsLeft = MaxTargets;
    }

    // Update is called once per frame
    void Update() {

        List<GameObject> targets = AcquireTargets();

        if (targets.Count != 0)
        {

            // Look at first enemy in list
            Vector3 lookat = targets[0].transform.position;
            lookat.y = transform.position.y;
            Quaternion oldRotation = transform.rotation;
            transform.LookAt(lookat);
            Quaternion newRotation = transform.rotation;
            transform.rotation = Quaternion.Lerp(oldRotation, newRotation, 0.1f);

            // Loop through targets, sending projectile at each
            if (TimeLeft <= 0)
            {
                targets.ForEach(Attack);
            }

            Tick();
        }
        else
        {
            // @TODO: Idle animation
        }

    }

    List<GameObject> AcquireTargets()
    {
        List<GameObject> targets = new List<GameObject>();
        GameObject[] potentialTargets = GameObject.FindGameObjectsWithTag("Enemy");

        // @TODO: First/Last are fairly easy to account for,
        // but we might have to create separate methods at some point
        // to deal with weakest/strongest
        int i = Priority == TargetingPriority.First ? 0 : potentialTargets.Length - 1;
        int inc = Priority == TargetingPriority.First ? 1 : -1;

        for (int j = 0; j < potentialTargets.Length; i += inc, j++)
        {
            GameObject enemy = potentialTargets[i];
            float distance = Vector3.Distance(enemy.transform.position, transform.position);
            if (distance <= Range)
            {
                if (targets.Count < GetMaxTargets())
                {
                    targets.Add(enemy);
                }
            }
        }

        return targets;
    }

	void Attack(GameObject target) {
        GameObject bullet = (GameObject)Instantiate(Projectile, transform.position, Quaternion.identity);
        bullet.GetComponent<Tracking>().SetTarget(target);
        bullet.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }

    int GetMaxTargets()
    {
        return Mode == AttackMode.MULTIPLE ? MaxTargets : 1;
    }

    void Tick()
    {
        if (TimeLeft <= 0)
        {
            TimeLeft = Cooldown;
        }
        else
        {
            TimeLeft -= Time.deltaTime;
        }
    }
}
