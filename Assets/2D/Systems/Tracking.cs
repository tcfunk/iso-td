﻿using UnityEngine;

public class Tracking : MonoBehaviour {

	public float Speed;
	private GameObject target;

    int JumpsLeft;

    private void Start()
    {
        JumpsLeft = 0;
    }

	// Update is called once per frame
	void Update () {
		// seek target
		if (target) {
            // Rotate body in direction of velocity
            transform.LookAt(target.transform);
            transform.position = transform.position + transform.forward.normalized * Speed * Time.deltaTime;
        } else {
            if (JumpsLeft > 0)
            {
            }
			Despawn();
		}
	}

	public void SetTarget(GameObject newTarget) {
		target = newTarget;
	}

	void Despawn() {
		Destroy(this.gameObject);
	}

}
