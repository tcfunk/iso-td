﻿using UnityEngine;

public class Targeting : MonoBehaviour
{

    public enum TargetingPriority { First, Last, Weakest, Strongest, Fixed };

    public GameObject Target;
    public float Range;

    public TargetingPriority Priority;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Target != null)
        {
            // Make sure target hasn't left range, drop target if so
            if ((Target.transform.position - transform.position).magnitude > Range)
            {
                SendMessageUpwards("LoseTarget", null);
            }
        }
        else
        {
            AcquireTarget();
        }
    }

    private void AcquireTarget()
    {
        GameObject newTarget = null;

        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            float distance = Vector3.Distance(enemy.transform.position, transform.position);
            if (distance <= Range)
            {
                newTarget = enemy;
                if (Priority == TargetingPriority.First) break;
            }
        }

        if (newTarget != null)
        {
            SendMessageUpwards("SetTarget", newTarget);
        }
    }

    void LoseTarget() {
        Target = null;
    }

    void SetTarget(GameObject newTarget) {
        Target = newTarget;
    }
}