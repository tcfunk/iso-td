﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour {

    public Material PassiveMaterial;
    public Material HighlightMaterial;
    public MeshRenderer TileRenderer;

    // Position in grid
    private int x, y;

    // Use this for initialization
    void Start() {
    }

    // Update is called once per frame
    void Update() {
    }

    public void SetTilePosition(Vector2 xy) {
        this.x = (int) xy.x;
        this.y = (int) xy.y;
        this.transform.position = new Vector3(this.x, 0, this.y);
        this.name =  "Tile " + x + "," + y;
    }

    public void Highlight() {
        TileRenderer.material = HighlightMaterial;
    }

    public void UnHighlight() {
        TileRenderer.material = PassiveMaterial;
    }
}
