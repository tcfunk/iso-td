using UnityEngine;
using System.Collections;

public class TileMap : MonoBehaviour
{
    // "static" class
    public static TileMap Instance { get; protected set; }

    // Dimensions of the map
    public int GridSize;

    // Tile GameObject to populate the map with
    public GameObject TilePrototype;

    GameObject mHovered;

    // Use this for initialization
    void Start()
    {
        if (Instance != null) {
            Debug.LogError("There should be only one TileMap");
            return;
        }
    }

    // Update is called once per frame
    void Update() {
        Ray pointer = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(pointer.origin, pointer.direction*50000, Color.yellow);


        // Check if a tile is hovered
        RaycastHit hit;
        GameObject hovered = null;
        if (Physics.Raycast(pointer, out hit)) {
            hovered = hit.transform.parent.gameObject;
            hovered.SendMessage("Highlight");
        }

        // UnHighlight the previously hovered tile
        if (hovered != mHovered) {
            if (mHovered != null) {
                mHovered.SendMessage("UnHighlight");
            }

            mHovered = hovered;
        }
    }
}
