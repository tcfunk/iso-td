using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    // Panning speed
    public float MoveSpeed;
    public float RotateDuration = 0.5f;

    // Cached version of main camera
    Camera cam;

    // Rotation vars
    bool isRotating = false;
    float startTime = 0f;
    float startAngle = 0f;
    float lastStep = 0f;


    void Start() {
        cam = Camera.main;
    }

    void Update() {

        // Rotate camera around origin
        if (isRotating) {
            Rotate();

        } else {
            if (Input.GetKeyDown(KeyCode.LeftArrow)) {
                RotateLeft();
            }

            else if (Input.GetKeyDown(KeyCode.RightArrow)) {
                RotateRight();
            }
        }

        Pan();

        // Zoom
        if (Input.GetKey(KeyCode.PageDown)) {
            cam.orthographicSize += 0.1f;

        } else if (Input.GetKey(KeyCode.PageUp)) {
            cam.orthographicSize -= 0.1f;

        }

    }

    void RotateRight() {
        StartRotation(90);
    }

    void RotateLeft() {
        StartRotation(-90);
    }

    private void StartRotation(float dt) {
        isRotating = true;
        startAngle = dt;
        lastStep = dt;
        startTime = Time.time;
    }

    private void Rotate() {
        float t = (Time.time - startTime) / RotateDuration;
        float step = Mathf.SmoothStep(startAngle, 0f, t);
        float inc = lastStep - step;
        lastStep = step;
        cam.transform.RotateAround(Vector3.zero, Vector3.up, inc);

        if (inc == 0) {
            isRotating = false;
        }
    }

    private void Pan() {
        Vector3 movement = Vector3.zero;

        // Forward-backward movemnt
        if (Input.GetKey(KeyCode.W)) {
            movement = cam.transform.forward;
        } else if (Input.GetKey(KeyCode.S)) {
            movement = -cam.transform.forward;
        }

        // Left-right movement
        if (Input.GetKey(KeyCode.D)) {
            movement += cam.transform.right;
        } else if (Input.GetKey(KeyCode.A)) {
            movement += -cam.transform.right;
        }
        
        movement.y = 0;
        cam.transform.position += movement.normalized * Time.deltaTime * MoveSpeed;
    }
}
